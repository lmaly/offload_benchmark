/*
 * Copyright (c) 2017, IT4Innovations National Supercomputing Center,
 * VSB-Technical University of Ostrava
 *
 * This file is part of Offload_benchmark program.
 *
 * Offload_benchmark is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * Offload_benchmakr is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */


#if defined USE_COI && defined USE_HAM
#pragma offload_attribute( push, target( mic ) )
#endif

//#include <stdint.h>
#include <iostream>
#include <cstdlib>

// Helper data pack for COI offload.
// Used to send necessary information via COIPipelineRunFunction
// for COI initialization on mics.
typedef struct coi_set_data
{
  // calculation type gemv, symv or spmv
  int calc_type;
  // dimentions of the matrix
  long mat_dim;
  // matrix size
  long mat_size;
  // pointer address to A matrices data in Ax=y
  uint64_t engine_data;
  unsigned int num_engines;
} coi_set_data;

void printDelim(){
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
}

//! prints vetor
void print_vector(
  long n,
  double * y 
  ){
  
  for( int i = 0; i < n; ++i ){
    std::cout << y[ i ] << " ";
  }
  std::cout << std::endl;
}

#if defined USE_COI && defined USE_HAM
#pragma offload_attribute( pop )
#endif

//! generates random matrix and vector data
void generate_data(
  long n,
  int type, // 0,1 ... full, 2 ... packed
  double * A,
  double * x
  ){

  long counter = 0;
  long ind;
  for( long j = 0; j < n; ++j ){
    x[ j ] = ( 7 * j + 3 ) % 11 / 11.0;
    for( long i = j; i < n; ++i ){
      if( type != 2 ){
        ind = i + j * n;
      } else {
        ind = counter;
      }
      A[ ind ] = ( 3 * i + 5 * j + 7 ) % 13 / 13.0;
      if( i != j && type != 2 ){
        A[ j + i * n ] = A[ ind ];
      }
      ++counter;
    }
  }
}

//! generates random matrix and vector data
void generate_data(
  long n,
  long n_mat,
  int type, // 0,1 ... full, 2 ... packed
  double * AA,
  double * xx
  ){

  long counter = 0;
  long ind;
  for( long k = 0; k < n_mat; ++k ){
    for( long j = 0; j < n; ++j ){
      xx[ j + k * n ] = ( 7 * j + 5 * k + 3 ) % 11 / 11.0;
      for( long i = j; i < n; ++i ){
        if( type != 2 ){
          ind = i + j * n + k * n * n;
        } else {
          ind = counter;
        }
        AA[ ind ] = ( 3 * i + 5 * j + 7 * k + 9 ) % 13 / 13.0;
        if( i != j && type != 2 ){
          AA[ j + i * n + k * n * n ] = AA[ ind ];
        }
        ++counter;
      }
    }
  }
}


