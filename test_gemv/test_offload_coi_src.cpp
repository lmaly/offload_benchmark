/*
 * Copyright (c) 2017, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

#include <iostream>
#include <map>
#include <string>
#include <cstring>
#include "aux.h"
#include <sys/time.h>
#include <stdexcept>
#include <string>

// COI related includes
#include <intel-coi/source/COIProcess_source.h>
#include <intel-coi/source/COIEngine_source.h>
#include <intel-coi/source/COIPipeline_source.h>
#include <intel-coi/source/COIEvent_source.h>
#include <intel-coi/source/COIBuffer_source.h>

// This benchmark is designed to work on 2 offload nodes.
// Change this number in order to run in on different number of nodes.
#define N_MICS ( 2 )
#define MIN_CMDLINE_ARGC 5

// Check if COI call returned COI_SUCCESS. Throws runtime_error in in case COI call failed.
#define COI_CALL_CHECK_RESULT(_COIFUNC){ \
  COIRESULT result = _COIFUNC; \
  if (result != COI_SUCCESS){ \
    std::cerr << #_COIFUNC << " returned " << COIResultGetName(result) << std::endl;\
    throw std::runtime_error("COI call failed"); \
  } \
}

// Helper struct containing all offload data needed by COI.
typedef struct coi_offload_data{
  // Name of offloading process. Used in COIProcessCreateFromFile.
  std::string sink_name;
  // Number of pipelines to run functions on targets.
  static const unsigned pipelines_num = 180;
  // The A matrices in Ax=y. 
  double *Apack;
  // The x vectors in Ax=y.
  double *xpack;
  // The y vectors in Ax=y.
  double *ypack;

  long n;
  long n_mat;
  int type;
  long mat_size;
  // Process handles. One per mic.
  COIPROCESS process_handles[N_MICS];
  // Buffers - one per process, i.e. one per engine.
  COIBUFFER buffer_handles[N_MICS];
  // Map function names (std::string) to function handles.
  // Needed because each process can have different function handle.
  std::map<std::string, COIFUNCTION> function_names_to_handles[N_MICS];
  // Use regular array of pipelines to enable concurrent access.
  COIPIPELINE pipeline_handles[N_MICS][pipelines_num];
  // We keep only the last event in the chain for each pipeline
  // this is because pipelines are FIFOs wrt executed functions
  COIEVENT function_completion_events[N_MICS][pipelines_num];

  long mat_per_engine[N_MICS];
  long *mat_index_per_engine;
  int  *mat_to_engine_map;
} coi_offload_data;

// Initialize coi_data structure and connections to card.
void init_coi(long n, long n_mat, int type, double * Apack,
  double * xpack, double * ypack, coi_offload_data * coi_data);

void iter_cpu_mic_coi(int n_iter, long n, long n_mat, int type, double * Apack,
  double * xpack, double * ypack, coi_offload_data * coi_data);

void calc_coi(long matNum, coi_offload_data * coi_data);

// Deallocate memory initialized for COI. Close COI connections.
void cleanup_coi(coi_offload_data * coi_data);


int main(
  int argc,
  char ** argv
  ){

  if (argc < MIN_CMDLINE_ARGC){
    //! function parameters
    std::cout << "-test_gemv type mat_size n_mat n_iter" << std::endl;
    return 0;
  }

  int type = atoi(argv[1]);
  long n = atoi(argv[2]);
  long n_mat = atoi(argv[3]);
  int n_iter = atoi(argv[4]);

  std::cout << "type: ";
  if (type == 0)
    std::cout << "gemv, ";
  if (type == 1)
    std::cout << "symv, ";
  if (type == 2)
    std::cout << "spmv, ";
  std::cout << "n: " << n << ", n_mat: " << n_mat << ", n_iter: " <<
    n_iter << std::endl;


  long Asize = n * n;
  if (type == 2) Asize = n * (n + 1) / 2;
  double *A, *x, *y, *Apack, *xpack, *ypack;
  coi_offload_data * coi_data = nullptr;

  try{
    A = new double[Asize];
    x = new double[n];
    y = new double[n];
    Apack = new double[n_mat * Asize];
    xpack = new double[n_mat * n];
    ypack = new double[n_mat * n];
    coi_data = new coi_offload_data();
  }
  catch (std::bad_alloc & exp){
    std::cerr << "Failed to allocate memory for initial data: " 
      << exp.what() << std::endl;
    std::exit(-1);
  }

  memset(y, 0.0, n * sizeof(double));
  memset(ypack, 0.0, n * n_mat * sizeof(double));
  generate_data(n, n_mat, type, Apack, xpack);

  iter_cpu_mic_coi(n_iter, n, n_mat, type, Apack, xpack, ypack, coi_data);
  //print_vector( n*n_mat, ypack );

  delete[] A;
  delete[] x;
  delete[] y;
  delete[] Apack;
  delete[] xpack;
  delete[] ypack;
  delete coi_data;

  return 0;
}

void init_coi(long n, long n_mat, int type, double * Apack,
  double * xpack, double * ypack, coi_offload_data * coi_data){

  coi_data->sink_name = "COI_sink";
  const char *sink_dir = getenv("COI_SINK_DIR");
  if (sink_dir && strlen(sink_dir) > 0){
    coi_data->sink_name = std::string(sink_dir) + "/" + coi_data->sink_name;
  }
  memset(coi_data->pipeline_handles, 0, sizeof(coi_data->pipeline_handles));

  coi_data->mat_size = (type == 2 ? n * (n + 1) / 2 : n * n);
  for (int mic_index = 0; mic_index < N_MICS; mic_index++){
    coi_data->mat_per_engine[mic_index] = n_mat / N_MICS;
  }

  for (long mic_index = 0; mic_index < n_mat % N_MICS; mic_index++){
    coi_data->mat_per_engine[mic_index]++;
  }

  try{
    coi_data->mat_index_per_engine = new long[n_mat];
    coi_data->mat_to_engine_map = new int[n_mat];
  }
  catch (std::bad_alloc & exp){
    std::cerr << "Failed to allocate memory for coi data: "
      << exp.what() << std::endl;
    std::exit(-1);
  }

  long mat = 0;
  for (int engine_index = 0; engine_index < N_MICS; engine_index++){
    for (long mat_index = 0;
        mat_index < coi_data->mat_per_engine[engine_index];
        mat_index++){
      coi_data->mat_index_per_engine[mat] = mat_index;
      coi_data->mat_to_engine_map[mat] = engine_index;
      mat++;
    }
  }

  coi_data->Apack = Apack;
  coi_data->xpack = xpack;
  coi_data->ypack = ypack;
  coi_data->n = n;
  coi_data->n_mat = n_mat;
  coi_data->type = type;

  unsigned num_engines;
  // Check how many targers are available.
  COI_CALL_CHECK_RESULT(COIEngineGetCount(COI_ISA_MIC, &num_engines));
  if (num_engines < 1 || num_engines != N_MICS){
    std::cerr << "Unexpected number of engines. Expected " << N_MICS
      << ". Got " << num_engines << std::endl;
    std::exit(-1);
  }

  for (unsigned engine_index = 0; engine_index < num_engines; engine_index++){
    // Connect to each engine.
    COIENGINE engine_handle;
    COI_CALL_CHECK_RESULT(COIEngineGetHandle(COI_ISA_MIC, engine_index,
      &engine_handle));
    // Create one process on each engine.
    COIPROCESS process_handle;
    COI_CALL_CHECK_RESULT(COIProcessCreateFromFile(engine_handle,
      coi_data->sink_name.c_str(), 0, nullptr, true, nullptr, false, nullptr, 0,
      nullptr, &process_handle));
    // There is one process per engine, so we are using engine index here too.
    coi_data->process_handles[engine_index] = process_handle;
    // Get function handles for the process.
    const char* func_names[] = { "SetType", "Calculate" };
    for (auto func_name : func_names){
      COIFUNCTION function_handle;
      COI_CALL_CHECK_RESULT(COIProcessGetFunctionHandles(process_handle, 1,
        &func_name, &function_handle));
      coi_data->function_names_to_handles[engine_index][func_name] =
        function_handle;
    }

    // Create pipelines - doesn't make sense to parallelize because
    // it gets serialized on the target side anyway
    for (unsigned pipeline_index = 0;
      pipeline_index < coi_data->pipelines_num;
      pipeline_index++){
      COI_CALL_CHECK_RESULT(COIPipelineCreate(coi_data->process_handles[engine_index],
        0, 0, &coi_data->pipeline_handles[engine_index][pipeline_index]));
    }
  }

#pragma omp parallel num_threads(N_MICS)
  #pragma omp for
  for (int engine_index = 0; engine_index < N_MICS; engine_index++){
    double *engine_data = coi_data->Apack;

    for (int i = 0; i < engine_index; i++){
      engine_data += coi_data->mat_per_engine[i] * coi_data->mat_size;
    }

    // Create COI buffer per process
    COI_CALL_CHECK_RESULT(COIBufferCreateFromMemory(
      coi_data->mat_per_engine[engine_index] * coi_data->mat_size * sizeof(double),
      COI_BUFFER_OPENCL,
      COI_OPTIMIZE_HUGE_PAGE_SIZE,
      engine_data,
      1,
      &(coi_data->process_handles[engine_index]),
      &(coi_data->buffer_handles[engine_index])));

    // Move the buffer data (with COI_BUFFER_MOVE)
    // to target (process handle provided in SetState).
    COI_CALL_CHECK_RESULT(COIBufferSetState(
      coi_data->buffer_handles[engine_index],
      coi_data->process_handles[engine_index],
      COI_BUFFER_VALID, COI_BUFFER_MOVE,
      0, NULL, 0));
    
    coi_set_data data;
    data.mat_dim = n;
    data.mat_size = coi_data->mat_size;
    data.calc_type = type;
    data.num_engines = N_MICS;

    COI_CALL_CHECK_RESULT(COIBufferGetSinkAddressEx(coi_data->process_handles[engine_index],
      coi_data->buffer_handles[engine_index], &data.engine_data));
    COI_CALL_CHECK_RESULT(COIPipelineRunFunction(
      coi_data->pipeline_handles[engine_index][0], // configure always using first pipeline on each engine
      coi_data->function_names_to_handles[engine_index]["SetType"],
      0,
      nullptr,
      nullptr,
      0,
      nullptr,
      (void *)&data,
      sizeof(data),
      nullptr,
      0,
      nullptr
      ));
  }
}

void iter_cpu_mic_coi(int n_iter, long n, long n_mat, int type, double * Apack,
  double * xpack, double * ypack, coi_offload_data * coi_data){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim();
  std::cout << "iter_cpu_mic_coi ";

  switch (type){
  case 0:
    std::cout << "cblas_dgemv started" << std::endl;
    break;
  case 1:
    std::cout << "cblas_dsymv started" << std::endl;
    break;
  case 2:
    std::cout << "cblas_dspmv started" << std::endl;
    break;
  }
  gettimeofday(&t0, nullptr);
  
  init_coi(n, n_mat, type, Apack, xpack, ypack, coi_data);

  gettimeofday(&t1, nullptr);
  init_t = (t1.tv_sec - t0.tv_sec) * 1000.0;
  init_t += (t1.tv_usec - t0.tv_usec) / 1000.0;

  std::cout << "warm-up";
  for (int i = 0; i < 2; ++i){
    calc_coi(n_mat, coi_data);
    std::cout << ".";
  }
  std::cout << std::endl;
  std::cout << "CALCULATIONS" << std::endl;
  gettimeofday(&t1, nullptr);
  
  for (int i = 0; i < n_iter; ++i){
    gettimeofday(&t3, nullptr);
    calc_coi(n_mat, coi_data);
    gettimeofday(&t4, nullptr);
    elapsed = (t4.tv_sec - t3.tv_sec) * 1000.0;
    elapsed += (t4.tv_usec - t3.tv_usec) / 1000.0;
    std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }

  gettimeofday(&t2, nullptr);

  cleanup_coi(coi_data);

  elapsed = (t2.tv_sec - t0.tv_sec) * 1000.0;
  elapsed += (t2.tv_usec - t0.tv_usec) / 1000.0;
  iter_t = (t2.tv_sec - t1.tv_sec) * 1000.0;
  iter_t += (t2.tv_usec - t1.tv_usec) / 1000.0;

  switch (type){
  case 0:
    std::cout << "cblas_dgemv finished, elapsed time: " <<
      elapsed << " ms, init time: " << init_t << " ms, iter time: " <<
      iter_t << " ms." << std::endl;
    break;
  case 1:
    std::cout << "cblas_dsymv finished, elapsed time: " <<
      elapsed << " ms, init time: " << init_t << " ms, iter time: " <<
      iter_t << " ms." << std::endl;
    break;
  case 2:
    std::cout << "cblas_dspmv finished, elapsed time: " <<
      elapsed << " ms, init time: " << init_t << " ms, iter time: " <<
      iter_t << " ms." << std::endl;
    break;
  }
  
  printDelim();
}

void calc_coi(long mat_num, coi_offload_data * coi_data){
#pragma omp parallel
  {
#pragma omp for
    for (long i = 0; i < mat_num; ++i){
      int engine_index = coi_data->mat_to_engine_map[i];
      int pipeline_index = i % coi_data->pipelines_num;

      double func_data[coi_data->n + 1];
      memcpy(func_data, coi_data->xpack + i * coi_data->n, sizeof(double) * coi_data->n);
      func_data[coi_data->n] = (double)coi_data->mat_index_per_engine[i];

      // Run the functions on targets asynchronously.
      COI_CALL_CHECK_RESULT(COIPipelineRunFunction(
        coi_data->pipeline_handles[engine_index][pipeline_index],
        coi_data->function_names_to_handles[engine_index]["Calculate"],
        0,
        nullptr,
        nullptr,
        0,
        nullptr,
        (void *)func_data,
        sizeof(double) * (coi_data->n + 1),
        (void *)(coi_data->ypack + i * coi_data->n),
        sizeof(double) * coi_data->n,
        &coi_data->function_completion_events[engine_index][pipeline_index]
        ));
    }
  }

  // Wait for each function to finish it's work.
  for (int engine_index = 0; engine_index < N_MICS; engine_index++){
    for (int pipeline_index = 0;
        pipeline_index < coi_data->pipelines_num;
        pipeline_index++){
      if (coi_data->pipeline_handles[engine_index][pipeline_index] != nullptr){
        COI_CALL_CHECK_RESULT(COIEventWait(1,
          &(coi_data->function_completion_events[engine_index][pipeline_index]),
          -1, 1, nullptr, nullptr));
      }
    }
  }
}

void cleanup_coi(coi_offload_data * coi_data){
  for (int engine_index = 0; engine_index < N_MICS; ++engine_index){
    COI_CALL_CHECK_RESULT(COIBufferDestroy(coi_data->buffer_handles[engine_index]));
  }

  for (int engine_index = 0; engine_index < N_MICS; ++engine_index){
    for (int pipeline_index = 0;
        pipeline_index < coi_data->pipelines_num;
        pipeline_index++){
      COI_CALL_CHECK_RESULT(COIPipelineDestroy(
        coi_data->pipeline_handles[engine_index][pipeline_index]));
    }
    signed char sink_return;
    unsigned exit_reason;
    COI_CALL_CHECK_RESULT(COIProcessDestroy(coi_data->process_handles[engine_index],
      -1, 0, &sink_return, &exit_reason));
  }
  delete[] coi_data->mat_index_per_engine;
  delete[] coi_data->mat_to_engine_map;
}


