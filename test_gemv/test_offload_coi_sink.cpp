/*
 * Copyright (c) 2017, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

#include <iostream>
#include <mkl.h>
#include "aux.h"

#include <intel-coi/sink/COIPipeline_sink.h>
#include <intel-coi/sink/COIProcess_sink.h>
#include <intel-coi/common/COIMacros_common.h>
#include <intel-coi/common/COISysInfo_common.h>
#include <intel-coi/common/COIEvent_common.h>

static int calc_type = 0;
static long mat_dim = 0;
static long mat_size = 0;
static double * engine_data = nullptr;
static unsigned int num_engines = 0;

// Sets configuration of the given target (engine)
COINATIVELIBEXPORT
void SetType(uint32_t in_BufferCount, void **in_ppBufferPointers,
  uint64_t *in_pBufferLengths, void *in_pMiscData, uint16_t in_MiscDataLength,
  void *in_pReturnValue, uint16_t in_ReturnValueLength){

  coi_set_data *data;
  if (in_MiscDataLength != sizeof(*data)){
    std::exit(-1);
  }
  data = (coi_set_data*)in_pMiscData;
  calc_type = data->calc_type;
  mat_dim = data->mat_dim;
  engine_data = (double*)data->engine_data;
  num_engines = data->num_engines;
  mat_size = data->mat_size;
}


COINATIVELIBEXPORT
void Calculate(uint32_t in_BufferCount, void **in_ppBufferPointers,
  uint64_t *in_pBufferLengths, void *in_pMiscData, uint16_t in_MiscDataLength,
  void *in_pReturnValue, uint16_t in_ReturnValueLength){

  double *x_vector = (double*)in_pMiscData;
  double *y_vector = (double*)in_pReturnValue;
  long mat_num = (long)x_vector[mat_dim];

  double *data = engine_data + mat_num * mat_size;
  switch (calc_type){
  case 0:
    mkl_set_threading_layer(MKL_THREADING_SEQUENTIAL);
    cblas_dgemv(CblasColMajor, CblasNoTrans, mat_dim, mat_dim, 1.0, data, mat_dim, x_vector, 1, 0.0, y_vector, 1);
    break;

  case 1:
    mkl_set_threading_layer(MKL_THREADING_SEQUENTIAL);
    cblas_dsymv(CblasColMajor, CblasLower, mat_dim, 1.0, data, mat_dim, x_vector, 1, 0.0, y_vector, 1);
    break;

  case 2:
    mkl_set_threading_layer(MKL_THREADING_SEQUENTIAL);
    cblas_dspmv(CblasColMajor, CblasLower, mat_dim, 1.0, data, x_vector, 1, 0.0, y_vector, 1);
    break;

  default:
    std::cout << "Error! Unknown calc_type!" << std::endl << std::flush;
    std::exit(-2);
  }
}

int main(int argc, char **argv){

  COIRESULT result = COIPipelineStartExecutingRunFunctions();
  if (COI_SUCCESS != result){
    std::cerr << "Failed to process pipeline function\n";
    std::exit(-1);
  }
  COIProcessWaitForShutdown();
  return 0;
}
