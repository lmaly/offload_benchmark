/*
 * Copyright (c) 2017, IT4Innovations National Supercomputing Center, 
 * VSB-Technical University of Ostrava
 *
 * This file is part of Offload_benchmark program.
 *
 * Offload_benchmark is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * Offload_benchmakr is distributed in the hope it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 */

//#define HAM_EXPLICIT

// -- includes --
#include <iostream>
#include <fstream>
#include <omp.h>
#include <sys/time.h>
#include <time.h>
#include <mkl.h>
#include "ham/offload.hpp"
#include "aux.h"

// function headings
void iter_cpu_mic_ham( int n_iter, long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack );

void xfer_to_mic_ham( long n, long n_mat, int type, double * Apack, 
  double * xpack, double * ypack, 
  ham::offload::buffer_ptr<double> micApack[],
  ham::offload::buffer_ptr<double> micXpack[], 
  ham::offload::buffer_ptr<double> micYpack[] );

void mv_cpu_mic_ham( long n, long n_mat, int type, double * Apack, double * xpack, 
  double * ypack, ham::offload::buffer_ptr<double> micApack[],
  ham::offload::buffer_ptr<double> micXpack[], 
  ham::offload::buffer_ptr<double> micYpack[] );

void dealloc_on_mic_ham(
  ham::offload::buffer_ptr<double> micApack[],
  ham::offload::buffer_ptr<double> micXpack[], 
  ham::offload::buffer_ptr<double> micYpack[] );

#define N_MICS ( 2 )

using namespace ham;

void test_gemv_mic_ham(
  int type,
  long n,
  long n_mat_i,
  offload::buffer_ptr<double> A_ptr,
  offload::buffer_ptr<double> x_ptr,
  offload::buffer_ptr<double> y_ptr
  ){

  long one = 1;
  double * A, * x, * y;

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  //std::cout << "mic: omp num threads = " << omp_get_max_threads() << std::endl;

  A = A_ptr.get( );
  x = x_ptr.get( );
  y = y_ptr.get( );

#pragma omp parallel 
{
  double * myA;
  double * myX;
  double * myY;

#pragma omp for
  for( int i = 0; i < n_mat_i; ++i ){
    myA = A + i * Asize;
    myX = x + i * n;
    myY = y + i * n;

    switch( type ){
      case 0:
        cblas_dgemv( CblasColMajor, CblasNoTrans, n, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break;
      case 1:
        cblas_dsymv( CblasColMajor, CblasLower, n, 1.0, myA, n, myX, one, 0.0, myY, one );
        break; 
      case 2:
        cblas_dspmv( CblasColMajor, CblasLower, n, 1.0, myA, myX, one, 0.0, myY, one );
        break;
    }
  }
} // omp parallel
}

//! main function
int main(
  int argc,
  char ** argv
  ){

/*
  bool init = offload::ham_init( argc, argv );
  if( !init ){
    std::cout << "ham offload could not be initialised -> exit()" << std::endl;
    return 0;
  }
*/
  std::cout << "test_HAM started -> " << argc << " cmd line arguments." << std::endl;

  if( argc < 5 ){
    //! function parameters
    std::cout << "-test_gemv type mat_size n_mat n_iter" << std::endl;
    return 0;
  }
  
  int type = atoi( argv[ 1 ] );
  long n = atoi( argv[ 2 ] );
  long n_mat = atoi( argv[ 3 ] );
  int n_iter = atoi( argv[ 4 ] );

  std::cout << "omp num threads = " << omp_get_max_threads() << std::endl;

  std::cout << "type: ";
  if( type == 0 )
    std::cout << "gemv, ";
  if( type == 1 )
    std::cout << "symv, ";
  if( type == 2 )
    std::cout << "spmv, ";
  std::cout << "n: " << n << ", n_mat: " << n_mat << ", n_iter: " << 
    n_iter << std::endl;

  long Asize = n * n;
  if( type == 2 ) Asize = n * ( n + 1 ) / 2;

  double * Apack = new double[ n_mat * Asize ];
  double * xpack = new double[ n_mat * n ];
  double * ypack = new double[ n_mat * n ];

  memset( ypack, 0.0, n * n_mat * sizeof( double ) );
  
  generate_data( n, n_mat, type, Apack, xpack );

  //! runs individual HAM offload
  iter_cpu_mic_ham( n_iter, n, n_mat, type, Apack, xpack, ypack );
  //print_vector( n*n_mat, ypack );

  delete [] Apack;
  delete [] xpack;
  delete [] ypack;

//  offload::ham_finalise();

  return 0;
}

void iter_cpu_mic_ham( 
  int n_iter, 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack 
  ){

  struct timeval t0, t1, t2, t3, t4;
  double elapsed, init_t, iter_t;

  printDelim( );

 switch( type ){
    case 0:
      std::cout << "ham-offload cblas_dgemv started" << std::endl;
      break;
    case 1:
      std::cout << "ham-offload cblas_dsymv started" << std::endl;
      break;
  case 2:
      std::cout << "ham-offload cblas_dspmv started" << std::endl;
      break;
  }
  gettimeofday( &t0, nullptr );

  offload::buffer_ptr<double> * micApack = new offload::buffer_ptr<double>[ N_MICS ]; 
  offload::buffer_ptr<double> * micXpack = new offload::buffer_ptr<double>[ N_MICS ];
  offload::buffer_ptr<double> * micYpack = new offload::buffer_ptr<double>[ N_MICS ];

  xfer_to_mic_ham( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, micYpack );

  gettimeofday( &t1, nullptr );
  
  init_t = ( t1.tv_sec - t0.tv_sec ) * 1000.0;
  init_t += ( t1.tv_usec - t0.tv_usec ) / 1000.0;

  //! warm-up
  std::cout << "warm-up ";
  for( int i = 0; i < 2; ++i ){
    mv_cpu_mic_ham( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, micYpack );
    std::cout << ".";
  }
  std::cout << std::endl;

  gettimeofday( &t1, nullptr );
 
  for( int i = 0; i < n_iter; ++i ){
     gettimeofday( &t3, nullptr );
     mv_cpu_mic_ham( n, n_mat, type, Apack, xpack, ypack, micApack, micXpack, micYpack );
     gettimeofday( &t4, nullptr );
     elapsed = ( t4.tv_sec - t3.tv_sec ) * 1000.0;
     elapsed += ( t4.tv_usec - t3.tv_usec ) / 1000.0;
     std::cout << " iteration time: " << elapsed << " ms." << std::endl;
  }
  
  gettimeofday( &t2, nullptr );
  
  dealloc_on_mic_ham( micApack, micXpack, micYpack );
  
  iter_t = ( t2.tv_sec - t1.tv_sec ) * 1000.0;
  iter_t += ( t2.tv_usec - t1.tv_usec ) / 1000.0;
  
  elapsed = ( t2.tv_sec - t0.tv_sec ) * 1000.0;
  elapsed += ( t2.tv_usec - t0.tv_usec ) / 1000.0;

  switch( type ){
    case 0:
      std::cout << "ham-offload cblas_dgemv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;
      break;
    case 1:
      std::cout << "ham-offload cblas_dsymv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;  
      break;
    case 2:
      std::cout << "ham-offload cblas_dspmv finished, elapsed time: "<< 
        elapsed << " ms, init time: " << init_t << " ms, iter time: "<<
        iter_t << " ms." << std::endl;        
      break;
  } 

  printDelim( ); 

}

void xfer_to_mic_ham( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack,
  offload::buffer_ptr<double> * micApack,
  offload::buffer_ptr<double> * micXpack,
  offload::buffer_ptr<double> * micYpack
  ){

  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ]; 
  long offset [ N_MICS ];
  long sizeall = n_mat / N_MICS;
  
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < N_MICS; ++i )
     offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];


#pragma omp parallel num_threads( N_MICS )
{
  double * myA;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < N_MICS; ++i ){
    offload::node_t target = i + 1;
     
    // allocate device memory
    micApack[ i ] = offload::allocate<double>( target, size[ i ] * Asize );
    micXpack[ i ] = offload::allocate<double>( target, size[ i ] * n );
    micYpack[ i ] = offload::allocate<double>( target, size[ i ] * n );

    myA = Apack + offset[ i ] * Asize;
    
    // sync matrix data transfer to appropriate sub_buffer
    offload::put( myA, micApack[ i ], size[ i ] * Asize );     
  }

} // omp parallel

}

void mv_cpu_mic_ham( 
  long n, 
  long n_mat, 
  int type, 
  double * Apack, 
  double * xpack, 
  double * ypack, 
  offload::buffer_ptr<double> micApack[],
  offload::buffer_ptr<double> micXpack[], 
  offload::buffer_ptr<double> micYpack[]
  ){
 
  long Asize = n * n;
  if ( type == 2 ) Asize = n * ( n + 1 ) / 2;

  long size [ N_MICS ]; 
  long offset [ N_MICS ];
  long sizeall = n_mat / N_MICS;
  
  for( int i = 0; i < N_MICS; ++i ){
    size[ i ] = sizeall;
    offset[ i ] = 0;
  }
  long rest = n_mat % N_MICS;
  for( long i = 0; i < rest; ++i )
    ++size[ i ];
  for( int i = 1; i < N_MICS; ++i )
    offset[ i ] = offset[ i - 1 ] + size[ i - 1 ];

#pragma omp parallel num_threads( N_MICS )
{

  double * myA;
  double * myX;
  double * myY;
  offload::node_t target;

#pragma omp for schedule( static, 1 )
  for( int i = 0; i < N_MICS; ++i ){
    myA = Apack + offset[ i ] * Asize;
    myX = xpack + offset[ i ] * n;
    myY = ypack + offset[ i ] * n;

    // transfer vector data to mic
    auto future_x = offload::put( myX, micXpack[ i ], size[ i ] * n );
    auto future_y = offload::put( myY, micYpack[ i ], size[ i ] * n );

    // synchronize
    future_x.get( );
    future_y.get( );

    target = micApack[ i ].node( );

    auto future_dgemv = offload::async( target, f2f(&test_gemv_mic_ham, 
      type, n, size[ i ], micApack[ i ], micXpack[ i ], micYpack[ i ] ));
    //void test_gemv_mic_ham(
    //int type,
    //long n,
    //long n_mat_i
    //offload::buffer_ptr<double> A_ptr,
    //offload::buffer_ptr<double> x_ptr,
    //offload::buffer_ptr<double> y_ptr)
      
    future_dgemv.get( );

    // transfer y data back to host
    offload::get( micYpack[ i ], myY, size[ i ] * n ); 
  }
} // omp parallel

}

void dealloc_on_mic_ham(
  offload::buffer_ptr<double> micApack[],
  offload::buffer_ptr<double> micXpack[], 
  offload::buffer_ptr<double> micYpack[] 
  ){

  for( int i = 0; i < N_MICS; ++i ){
    offload::free( micApack[ i ] );
    offload::free( micXpack[ i ] );
    offload::free( micYpack[ i ] );
  }

}


