#
# Copyright (c) 2017, IT4Innovations National Supercomputing Center,
# VSB-Technical University of Ostrava
#
# This file is part of Offload_benchmark program.
#
# Offload_benchmark is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU Lesser General Public License,
# version 2.1, as published by the Free Software Foundation.
#
# Offload_benchmark is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#

##                                                                     #
# Usage: ./run.sh  type mat_size n_mat n_iter n_streams_per_dom \      #
#            oversubscription                                          #
##                                                                     #
# Parameters:                                                          #
#   -type               .. 0 = dgemv, 1 = dsymv, 2 = dspmv             #
#   -mat_size           .. size of each matrix                         #
#   -n_mat              .. number of matrices                          #
#   -n_iter             .. number of itiration                         #
#   -n_streams_per_dom  .. hStream initial parameter                   #
#   -oversubscription   .. hStream initial parameter                   #
##                                                                     #
# Example: ./run.sh 0 500 2000 30 120 1                                #
#                                                                      #
##                                                                     #
# Makefile flags: (set in Makefile)                                    #
#   -mkl                                                               #
#   -mkl=sequential                                                    #


# Set environment variables below                                  

export MIC_ENV_PREFIX=MIC
export MIC_OMP_NESTED=false
export MIC_MKL_NUM_THREADS=1
export MIC_OMP_DYNAMIC=false
export MIC_MKL_DYNAMIC=false
export MIC_KMP_AFFINITY='scatter,granularity=core' 
export MIC_KMP_HW_SUBSET=60c,4t
#export MIC_KMP_AFFINITY=granularity=core,scatter
#export MIC_KMP_AFFINITY=norespect,granularity=core,scatter
#export MIC_OMP_PLACES=threads
#export MIC_OMP_PROC_BIND=spread,close
#export MIC_OFFLOAD_STREAM_AFFINITY=scatter

unset OFFLOAD_REPORT
#export OFFLOAD_REPORT=3

scriptpath="$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )"
host_bin_dir="$( cd "$scriptpath"/bin/host ; pwd -P)"
dev_bin_dir="$( cd "$scriptpath"/bin/x100-card ; pwd -P)"
mpss_lib_dir="/opt/mpss/$(rpm --qf %{VERSION} -q mpss-daemon)/sysroots/k1om-mpss-linux/usr/lib64/"

echo $scriptpath
echo $host_bin_dir
echo $dev_bin_dir
echo $mpss_lib_dir

export SINK_LD_LIBRARY_PATH=$mpss_lib_dir:$dev_bin_dir:$MIC_LD_LIBRARY_PATH
export HOST_SINK_LD_LIBRARY_PATH=$host_bin_dir:$LD_LIBRARY_PATH
# Relative path where COI_host looks for COI_sink binary
export COI_SINK_DIR=../x100-card

# Make this 4G for an 8G card
export MKL_MIC_MAX_MEMORY=8G
export MIC_USE_2MB_BUFFERS=64K

pushd $TUT_INSTALL/bin/host > /dev/null
pwd
# some or all of these arguments may be missing, and will be ignored
#./hStreams $1 $2 $3 $4 $5 $6
./LEO $1 $2 $3 $4
#./LEO_streams $1 $2 $3 $4 $5
export SINK_LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH
#./COI_host $1 $2 $3 $4

./HAM $1 $2 $3 $4 --ham-process-count 3 --ham-address 0 &
ssh mic0 env KMP_AFFINITY='scatter,granularity=core' KMP_HW_SUBSET=60c,4t LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH  `pwd`/../x100-card/HAM_mic --ham-process-count 3 --ham-address 1 &
ssh mic1 env KMP_AFFINITY='scatter,granularity=core' KMP_HW_SUBSET=60c,4t LD_LIBRARY_PATH=$MIC_LD_LIBRARY_PATH  `pwd`/../x100-card/HAM_mic --ham-process-count 3 --ham-address 2

popd > /dev/null
