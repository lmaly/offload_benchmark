# Acceleration of Matrix-Vector Multiplication using offloads
This repository serves as a benchmark for comparison of Intel Xeon Phi offload runtimes and libraries. Our implementation covers several different versions using Intel Language Extansions (LEO), Hetero Streams Library (hStreams), and Heterogeneous Active Messages (HAM). As a benchmark we use the dense matrix-vector multiplication.

--------------------------------------------------------------------------------
### Brief explanation of directory tree of source code distribution.

Directory                 | Description
:-------------------------|:----------------------------------------------------
./test_gemv               | Experimental code


--------------------------------------------------------------------------------
### Building and running the test code
Set up the Intel (R) C++ Composer XE environment. Then run make to build. Read run.sh file to run the application. 

