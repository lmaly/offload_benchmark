#
# Copyright (c) 2017, IT4Innovations National Supercomputing Center,
# VSB-Technical University of Ostrava
#
# This file is part of Offload_benchmark program.
#
# Offload_benchmark is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU Lesser General Public License,
# version 2.1, as published by the Free Software Foundation.
#
# Offload_benchmark is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#

.DEFAULT_GOAL := all

HSTR_INSTALL:=/usr/share/doc/hStreams
HAM_INSTALL:=/home/mal539/HAM_offload/ham

# Location for ref_code directory
HOME_DIR:=$(TUT_INSTALL)/
SRC_DIR:=test_gemv/

COMMON_CXXFLAGS = -w2 -fpic -O2 -std=c++11 -qopenmp
#COMMON_CXXFLAGS = -w1 -fpic -O3 -std=c++11 -qopenmp

HSTR_CXXFLAGS = -I/usr/include/hStreams
HAM_CXXFLAGS = -DHAM_COMM_SCIF  -DUSE_HAM 
HAM_CXXFLAGS += -I/home/mal539/HAM_offload/ham/include
HAM_CXXFLAGS += -I/home/mal539/Software/boost/include

HOST_CXXFLAGS    := $(COMMON_CXXFLAGS)
x100_SINK_CXXFLAGS := $(COMMON_CXXFLAGS)

# We use icpc for compilation
HOST_CXX    := icpc
x100_SINK_CXX := icpc -mmic

# Tags which to use for namng the intermediate object files Helps avoid name
# clashes among compilations for different targets from one source file.
HOST_TAG    := source
x100_SINK_TAG := x100-sink

# dir creation guard, to prevent explicitly adding directories as targets
# put $(dir_create) as a first command in the target definition
dir_create=@mkdir -p $(@D)

RM_rf := rm -rf

BIN_HOST = $(HOME_DIR)/bin/host/
BIN_x100 = $(HOME_DIR)/bin/x100-card/
BUILD_HOST = $(HOME_DIR)/build/host/
BUILD_x100 = $(HOME_DIR)/build/x100-card/

TARGET_HSTR := $(BIN_HOST)hStreams
TARGET_LEO := $(BIN_HOST)LEO
TARGET_LEOSTR := $(BIN_HOST)LEO_streams
TARGET_COI_HOST := $(BIN_HOST)COI_host
TARGET_COI_SINK := $(BIN_x100)COI_sink
TARGET_HAM := $(BIN_HOST)HAM
TARGET_HAM_MIC := $(BIN_x100)HAM_mic

x100_SINK_TARGET_HSTR := $(BIN_x100)hStreams_mic.so

ADDITIONAL_HOST_CXXFLAGS := -xCORE-AVX2
ADDITIONAL_COI_CXXFLAGS := -DUSE_COI

#  -lhstreams_source is required for hStreams
ADDITIONAL_HOST_LDFLAGS  := -lhstreams_source -qopenmp -lmkl_intel_ilp64 \
	-lmkl_core -lmkl_sequential \
	-qoffload-option,mic,ld," -L${MIC_LD_LIBRARY_PATH} -rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ -rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core"

ADDITIONAL_LEO_LDFLAGS  := -qopenmp -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential \
	-qoffload-option,mic,ld," -L${MIC_LD_LIBRARY_PATH} -rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ -rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core"

# -lcoi_host is required for coi host applications
ADDITIONAL_COI_HOST_LDFLAGS := -qopenmp -lcoi_host -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential

# -lcoi_device is required for coi sink applications
ADDITIONAL_COI_SINK_LDFLAGS := -mmic -L$(MIC_LD_LIBRARY_PATH) -Wl,-rpath=$(MIC_LD_LIBRARY_PATH) -L${MKLROOT}/lib/mic/ -Wl,-rpath=${MKLROOT}/lib/mic/ -lcoi_device -rdynamic -Wl,--enable-new-dtags -lmkl_rt -fopenmp -std=c++11

# The soname is a string, which is used as a "logical name" describing the
# functionality of the object
ADDITIONAL_x100_SINK_HSTR_LDFLAGS:= -shared \
  -Wl,-soname,$(x100_SINK_TARGET_HSTR) -qopenmp -L${MIC_LD_LIBRARY_PATH} \
	-Wl,-rpath=${MIC_LD_LIBRARY_PATH} -L${MKLROOT}/lib/mic/ \
	-Wl,-rpath=${MKLROOT}/lib/mic/ -lmkl_intel_ilp64 \
	-lmkl_intel_thread -lmkl_core

# -lham_offload_scif or -lham_offload_mpi is reqiured for HAM
ADDITIONAL_HAM_HOST_LDFLAGS := -L"/home/mal539/Software/boost/lib" \
	-L"/home/mal539/HAM_offload/ham/bin/intel-linux/release/inlining-on/threading-multi" \
	-Wl,-R -Wl,"/home/mal539/HAM_offload/ham/bin/intel-linux/release/inlining-on/threading-multi" \
	-Wl,-R -Wl,"/home/mal539/Software/boost/lib" \
	-Wl,-rpath-link -Wl,"/home/mal539/HAM_offload/ham/bin/intel-linux/release/inlining-on/threading-multi" \
	-Wl,-rpath-link -Wl,"/home/mal539/Software/boost/lib" \
	-lham_offload_scif -parallel -lscif -lboost_program_options -lrt  -pthread -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential

ADDITIONAL_HAM_MIC_LDFLAGS := -parallel \
	-mmic -Wl,-rpath=$(MIC_LD_LIBRARY_PATH) \
	-L"/home/mal539/Software/boost_mic/lib" \
	-L"/home/mal539/HAM_offload/ham/bin/intel-linux/release_mic/inlining-on/threading-multi" \
	-Wl,-R -Wl,"/home/mal539/HAM_offload/ham/bin/intel-linux/release_mic/inlining-on/threading-multi" \
	-Wl,-R -Wl,"/home/mal539/Software/boost_mic/lib" \
	-L${MKLROOT}/lib/mic/ -Wl,-rpath=${MKLROOT}/lib/mic/ \
	-lham_offload_scif -lboost_program_options -lscif -ldl -lrt -pthread  -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential

HSTR_SRCS := test_hStreams_src.cpp
HSTR_OBJS := $(addprefix $(BUILD_HOST), $(HSTR_SRCS:.cpp=.$(HOST_TAG).o))

LEO_SRCS := test_LEO.cpp
LEO_OBJS := $(addprefix $(BUILD_HOST),  $(LEO_SRCS:.cpp=.$(HOST_TAG).o))

LEOSTR_SRCS := test_offload_streams.cpp
LEOSTR_OBJS := $(addprefix $(BUILD_HOST), $(LEOSTR_SRCS:.cpp=.$(HOST_TAG).o))

COI_HOST_SRCS := test_offload_coi_src.cpp
COI_HOST_OBJS := $(addprefix $(BUILD_HOST), $(COI_HOST_SRCS:.cpp=.$(HOST_TAG).o))

#HAM_SRCS := inner_product.cpp
HAM_SRCS := test_HAM.cpp
HAM_OBJS := $(addprefix $(BUILD_HOST), $(HAM_SRCS:.cpp=.$(HOST_TAG).o))
HAM_MIC_OBJS := $(addprefix $(BUILD_x100), $(HAM_SRCS:.cpp=.$(x100_SINK_TAG).o))

COI_SINK_SRCS := test_offload_coi_sink.cpp
COI_SINK_OBJS := $(addprefix $(BUILD_x100), $(COI_SINK_SRCS:.cpp=.$(x100_SINK_TAG).o))

x100_SINK_SRCS = test_hStreams_sink.cpp
x100_SINK_OBJS = $(addprefix $(BUILD_x100), $(x100_SINK_SRCS:.cpp=.$(x100_SINK_TAG).o))

# Target for hStreams tests
hStreams: $(TARGET_HSTR) $(x100_SINK_TARGET_HSTR)

# Target for LEO tests
LEO: $(TARGET_LEO)

# Target for offload streams tests
LEO_streams: $(TARGET_LEOSTR)

# Target for COI tests
COI: $(TARGET_COI_HOST) $(TARGET_COI_SINK)

# Target for HAM offlaod tests
HAM: $(TARGET_HAM) $(TARGET_HAM_MIC)

# The default "all" target - builds everything
all: hStreams LEO LEO_streams COI HAM

# objs
$(HSTR_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(HSTR_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(HSTR_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(LEO_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(LEO_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(LEOSTR_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(LEOSTR_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_HOST_CXXFLAGS)

$(COI_HOST_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(COI_HOST_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(ADDITIONAL_COI_CXXFLAGS) \
	$(ADDITIONAL_HOST_CXXFLAGS)

$(HAM_OBJS): %.$(HOST_TAG).o: $(SRC_DIR)$(HAM_SRCS)
	$(dir_create)
	$(HOST_CXX) -c $^ -o $@ $(HOST_CXXFLAGS) $(HAM_CXXFLAGS) $(ADDITIONAL_HAM_CXXFLAGS) \
	$(ADDITIONAL_HOST_CXXFLAGS)

$(x100_SINK_OBJS): %.$(x100_SINK_TAG).o: $(SRC_DIR)$(x100_SINK_SRCS)
	$(dir_create)
	$(x100_SINK_CXX) -c $^ -o $@ $(x100_SINK_CXXFLAGS) $(HSTR_CXXFLAGS)

$(COI_SINK_OBJS): %.$(x100_SINK_TAG).o: $(SRC_DIR)$(COI_SINK_SRCS)
	$(dir_create)
	$(x100_SINK_CXX) -c $^ -o $@ $(x100_SINK_CXXFLAGS) $(ADDITIONAL_COI_CXXFLAGS)

$(HAM_MIC_OBJS): %.$(x100_SINK_TAG).o: $(SRC_DIR)$(HAM_SRCS)
	$(dir_create)
	$(x100_SINK_CXX) -c $^ -o $@ $(x100_SINK_CXXFLAGS) $(HAM_CXXFLAGS) $(ADDITIONAL_HAM_CXXFLAGS)

# targets
$(x100_SINK_TARGET_HSTR): $(x100_SINK_OBJS)
	$(dir_create)
	$(x100_SINK_CXX) $^ -o $@ $(x100_SINK_LDFLAGS) \
	$(ADDITIONAL_x100_SINK_HSTR_LDFLAGS)

$(TARGET_COI_SINK): $(COI_SINK_OBJS)
	$(dir_create)
	$(x100_SINK_CXX) $^ -o $@ $(ADDITIONAL_COI_SINK_LDFLAGS) $(x100_SINK_LDFLAGS)

$(TARGET_HAM_MIC): $(HAM_MIC_OBJS)
	$(dir_create)
	$(x100_SINK_CXX) $^ -o $@ $(ADDITIONAL_HAM_MIC_LDFLAGS) $(x100_SINK_LDFLAGS)

$(TARGET_HSTR): $(HSTR_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_HOST_LDFLAGS)

$(TARGET_LEO): $(LEO_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_LEO_LDFLAGS)

$(TARGET_LEOSTR): $(LEOSTR_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_LEO_LDFLAGS)

$(TARGET_COI_HOST): $(COI_HOST_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_COI_HOST_LDFLAGS)

$(TARGET_HAM): $(HAM_OBJS)
	$(dir_create)
	$(HOST_CXX) $^ -o $@ $(HOST_LDFLAGS) \
	$(ADDITIONAL_HAM_HOST_LDFLAGS)

.PHONY: clean
clean:
	$(RM_rf) $(TARGET_HSTR) $(HSTR_OBJS) \
	$(x100_SINK_TARGET_HSTR) $(x100_SINK_OBJS) \
	$(TARGET_LEO) $(LEO_OBJS) \
	$(TARGET_LEOSTR) $(LEOSTR_OBJS) \
	$(TARGET_COI_HOST) $(COI_HOST_OBJS) \
	$(TARGET_COI_SINK) $(COI_SINK_OBJS) \
	$(TARGET_HAM) $(HAM_OBJS) \
	$(TARGET_HAM_MIC) $(HAM_MIC_OBJS)
